/*************************************************************************
	> File Name: JudgePrime.c
	> Author: llh
	> Mail: l3503231956@163.com 
	> Created Time: 2022年04月03日 星期日 23时00分32秒
 ************************************************************************/

#include<stdio.h>

int main(){
	long long n = 130784637447878569;	//判断整数n是否为质数
	int sign = 1;	//标记n是否为质数,默认成立

	if(n < 2){	//负数和 1 ，不在判断范围中
		sign = 0;
	}else if(n > 2){	//大于 2 的数据需要判断
		for(int i = 2; i < n; i++){
			if(n%i == 0){
				sign = 0;	//一定不是质数，置sign，并跳出循环
				break;	
			}
		}
	}

	if(sign){
		printf("%lld是一个质数！\n", n);
	} else {
		printf("%lld不是一个质数！\n", n);
	}

	return 0;
}
