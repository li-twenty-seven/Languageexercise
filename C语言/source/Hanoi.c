/*************************************************************************
	> File Name: Hanoi.c
	> Author: llh
	> Mail: l3503231956@163.com 
	> Created Time: 2022年04月06日 星期三 17时23分46秒
 ************************************************************************/
/*request:
 * 汉诺塔问题：有三根杆(编号A、B、C)，在A杆自下而上、由大到小按顺序放置64个金盘。游戏的目标：把A杆上的金盘全部移到C杆上，并仍保持原有顺序叠好。操作规则：每次只能移动一个盘子，并且在移动过程中三根杆上都始终保持大盘在下，小盘在上，操作过程中盘子可以置于A、B、C任一杆上；输入：金盘的个数；输出：要完成游戏每一步需要如何移动？
 */

#include<stdio.h>

void hanoi(int n, char a, char b, char c){
	if(n == 1) {
		printf("%c——>%c\t", a, c);
		return;
	} else {
		hanoi(n-1, a, c, b);
		printf("%c——>%c\t", a, c);
		hanoi(n-1,b, a, c);
		return;
	}
}

int main(){
	int n;
	scanf("%d", &n);
	printf("%d个金盘的移动步骤:\n", n);
	hanoi(n,'A','B','C');
	putchar('\n');
	return 0;
}
