/*************************************************************************
	> File Name: source/Euclid.c
	> Author: llh
	> Mail: l3503231956@163.com 
	> Created Time: 2022年05月06日 星期五 20时21分05秒
 ************************************************************************/
/*request:
 * 欧几里得算法实现求解最大公约数
 */
#include<stdio.h>

int gcd(int a, int b){
	if(b == 0) return a;
	else{
		gcd(b, a%b);
	} 
}

int main(){
	int a, b;
	scanf("%d %d", &a, &b);
	printf("%d\n", gcd(a,b));
	return 0;
}
