/*************************************************************************
	> File Name: JudgePrime1.c
	> Author: llh
	> Mail: l3503231956@163.com 
	> Created Time: 2022年04月03日 星期日 23时29分39秒
 ************************************************************************/

#include<stdio.h>
#include<math.h>

int main(){
	long long n;
	int sign = 1;	//标记n是否为质数,默认成立

	scanf("%lld", &n);	//优化2：窗口输入n
	if(n < 2){	//负数和 1 ，不在判断范围中
		sign = 0;
	}else if(n > 2){
		for(int i = 2; i < sqrt(n); i++){	//优化1：只需要判断到根号下n即可，大于根号n的数一定不能整除n
			if(n%i == 0){
				sign = 0;	//一定不是质数，置sign，并跳出循环
				break;	
			}
		}
	}

	if(sign){
		printf("%lld是一个质数！\n", n);
	} else {
		printf("%lld不是一个质数！\n", n);
	}

	return 0;
}
