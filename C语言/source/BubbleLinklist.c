/*************************************************************************
	> File Name: BubbleLinklist.c
	> Author: llh
	> Mail: l3503231956@163.com 
	> Created Time: 2022年04月28日 星期四 07时31分52秒
 ************************************************************************/
/*request：
* 输入一串数字，创建双向链表然后使用冒泡排序算法排序，注意：排序要交换节点，而不是交换节点中的值
*/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

struct Node{
    int val;
    struct Node* before;
    struct Node* next;
};

int main(){
    int len;
    int val;
    int i;
    struct Node* head, *tail, *cur, *temp;

    head = (struct Node*)malloc(sizeof(struct Node));
    head->before = NULL;
    head->next = NULL;
    head->val = 0;
    scanf("%d", &len);
    cur = head;
    for(i = 0; i < len; i++){
       scanf("%d", &val);
       tail = (struct Node*)malloc(sizeof(struct Node));
       cur->next = tail;
       tail->val = val;
       tail->before = cur;
       tail->next = NULL;
       cur = tail;
    }
   cur = head->next;
   while(cur != NULL){
       temp = cur->next;
       while(temp != NULL){
           if(cur->next != temp && cur->val < temp->val){
              struct Node* t;
              if(temp->next != NULL){
                  t = temp->next;
                  cur->before->next = temp;
                  if(cur->next != temp) temp->next = cur->next;
                  temp->before->next = cur;
                  cur->next = t;

                  t = cur->before;
                  cur->next->before = cur;
                  cur->before = temp->before;
                  temp->next->before = temp;
                  temp->before = t;
              } else {
                  t = temp->next;
                  cur->before->next = temp;
                  temp->next = cur->next;
                  temp->before->next = cur;
                  cur->next = t;

                  t = cur->before;
                  cur->before = temp->before;
                  temp->next->before = temp;
                  temp->before = t;
              }
              t = temp;
              temp = cur;
              cur = t;
           }
           if(cur->val < temp->val){
              struct Node* t;
              t = temp->next;
              cur->before->next = temp;
              temp->next = cur;
              cur->next = t;

              t = cur->before;
              if(cur->next != NULL){
                 cur->next->before = cur;
              }
              cur->before = temp;

              temp->before = t;
              t = temp;
              temp = cur;
              cur = t;
           }
           temp = temp->next;
       }
       cur = cur->next;
   }
   cur = head->next;
   while(cur != NULL){
       printf("%d ", cur->val);
       cur = cur->next;
   }
   puts("");
    return 0;
}


