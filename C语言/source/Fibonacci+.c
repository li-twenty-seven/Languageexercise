/*************************************************************************
	> File Name: Fibonacci+.c
	> Author: llh
	> Mail: l3503231956@163.com 
	> Created Time: 2022年04月06日 星期三 17时09分12秒
 ************************************************************************/
/*request:
 * 改进递归算法为非递归,实际上本题可以根据输入生成数组存储数列元素更为简洁
 */

#include<stdio.h>

int fib(int n){
	if(n <= 2) return 1;
	int n1 = 1;
	int n2 = 1;
	int n3 = 2;
	for(int i=3; i <= n; i++){
		n3 = n1 + n2;
		n1 = n2;
		n2 = n3;
	}
	return n3;
}

int main(){
	int n;
	scanf("%d", &n);
	for(int i=1; i <= n; i++){
		printf("%9d", fib(i));
		if(i%10==0) putchar('\n');
	}
	putchar('\n');
	return 0;
}
