/*************************************************************************
	> File Name: Daffodil.c
	> Author: llh
	> Mail: l3503231956@163.com 
	> Created Time: 2022年04月04日 星期一 00时41分24秒
 ************************************************************************/
/*request:
 *输出所有的水仙花数；水仙花数的定义是指一个 3 位数，它的每个位上的数字的 3次幂之和等于它本身（例如：1^3 + 5^3+ 3^3 = 153）
*/

#include<stdio.h>
#include<math.h>

int main(){
	int n =100;	  //遍历所有三位数
	int temp;
	int g,s,b;

	for(;n < 1000; n++){
		temp = n;
		g = temp % 10;
		temp /= 10;
		s = temp % 10;
		temp /= 10;
		b = temp % 10;

		g = pow(g, 3);
		s = pow(s, 3);
		b = pow(b, 3);
		if(g+s+b == n){
			printf("%d是一个水仙花数\n", n);
		}
	}
	
	return 0;
}
