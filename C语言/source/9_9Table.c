/*************************************************************************
	> File Name: 9_9Table.c
	> Author: llh
	> Mail: l3503231956@163.com 
	> Created Time: 2022年04月03日 星期日 23时57分26秒
 ************************************************************************/

#include<stdio.h>
int main(){
	int list, row;	//格式化输出九九乘法表的列、行
	
	for(list=1, row=1; row<=9 && list<=row; list++){    //从第一行开始，按行完整输出
		printf("%2d *%2d ", list, row);
		if(list == row){   //如果这一行输出结束了，需要重置列，移向下一行
			list = 1;
			row++;
			printf("\n");
		}
	}
	
	return 0;
}
