/*************************************************************************
	> File Name: source/GreatestCommonDivisor.c
	> Author: llh
	> Mail: l3503231956@163.com 
	> Created Time: 2022年05月06日 星期五 20时07分45秒
 ************************************************************************/
/*request
 *  输入两个正整数，返回他们的最大公约数(这里并没有使用欧几里得算法)
 */
#include<stdio.h>
int main(){
	int a, b, gcd=0, i;
	printf("输入两个正整数:");
	scanf("%d %d", &a, &b);
	for(i = 1; i <= a && i <= b; i++){
		if(a%i==0 && b%i == 0){
			gcd = i;
		}
	}
	printf("%d\n", gcd);
	return 0;
}
