/*************************************************************************
	> File Name: source/ThreadSort.c
	> Author: llh
	> Mail: l3503231956@163.com 
	> Created Time: 2022年04月28日 星期四 07时57分55秒
 ************************************************************************/
/*request：
* 利用多个线程实现对数组的排序，注意其中空指针的使用！！！
*/
#include<stdio.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>

#define N 2

struct A{
	int size;
	int* arry;
};

void sort(struct A* arry){
	int i, j, t,temp;
	for(i = 0; i < arry->size; i++){
		t = i;
		for(j = i + 1; j < arry->size; j++){
			if(arry->arry[j] < arry->arry[t]) t = j;
		}
		if(t != i){
			temp = arry->arry[t];
			arry->arry[t] = arry->arry[i];
			arry->arry[i] = temp;
		}
	}
	return;
}

void* thread(void* arg){
	sort((struct A*)arg);
	return (void*)arg;
}

int main(){
	int i, j;
	pthread_t tid[N];
	struct A a[N];
	int size = 9;
	int arry[] = {9, 8, 7, 6, 5, 4, 3, 2, 1};
	
	if(size == 1) {
		printf("%d", arry[0]);
		return 0;
	}

	for(i = 0; i < N; i++){
		if(i == N-1){
			a[i].size = size%N+size/N;
		} else {
			a[i].size = size/N;
		}
		a[i].arry = (int*)malloc(sizeof(int)*a[i].size);
		for(j = 0; j < a[i].size; j++){
			a[i].arry[j] = arry[j+i*(size/N)];
		}
		pthread_create(&tid[i], NULL, &thread, &a[i]);
	}
	for(i = 0; i < N; i++){
		pthread_join(tid[i], (void**)NULL);
	}
	int h1 = 0;
	int h2 = 0;
	int t = 0;
	while(!(h1 == a[0].size && h2 == a[1].size)){
		if(h1 < a[0].size && h2 < a[1].size){
			if(a[0].arry[h1] < a[1].arry[h2]){
				arry[t++] = a[0].arry[h1++];
			} else {
				arry[t++] = a[1].arry[h2++];
			}
		} else if(h1 < a[0].size){
			arry[t++] = a[0].arry[h1++];
		} else if(h2 < a[1].size){
			arry[t++] = a[1].arry[h2++];
		}
	}
	for(i = 0; i < size; i++) printf("%d ", arry[i]);
	puts("");
	return 0;
}

