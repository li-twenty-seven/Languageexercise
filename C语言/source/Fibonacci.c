/*************************************************************************
	> File Name: Fibonacci.c
	> Author: llh
	> Mail: l3503231956@163.com 
	> Created Time: 2022年04月06日 星期三 16时55分37秒
 ************************************************************************/
/*request:
 * 输入整数n，输出斐波那契数列1～n项（五项为一组对齐）
 */

#include<stdio.h>

//返回数列第i项的值
int fib(int i){
	if(i <= 2){//递归的边界控制
		return 1;
	} else {
		return fib(i-1)+fib(i-2);
	}
}

int main(){
	int n;
	scanf("%d", &n);
	for(int i=1; i <= n; i++){
		printf("%9d", fib(i));//输出第i项
		if(i % 10 == 0) putchar('\n');
	}
	putchar('\n');
	return 0;
}
