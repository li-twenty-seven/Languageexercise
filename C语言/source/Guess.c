/*************************************************************************
	> File Name: Guess.c
	> Author: llh
	> Mail: l3503231956@163.com 
	> Created Time: 2022年04月04日 星期一 00时10分28秒
 ************************************************************************/
/*request:
 *		系统随机生成一个1-10之间的数字，由客户猜，每次输入了猜的数字，系统都会进行猜对了或猜错了的提示，客户有9次猜选机会。（要求：用户永远都猜不中）
*/

#include<stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){
	int n[9];
	int sign;

	for(int i=0; i < 9; i++){	//猜测九次，每次输出提示
		printf("请输入您猜测的数据（1-10）：");
		scanf("%d", &n[i]);
		printf("猜错了！\n");
	}
	while(1){
		srand((unsigned)time(NULL));
		int r = rand()%10 + 1;
		sign = 1;
		for(int i = 0; i<9;i++){
			if(r == n[i]){
				sign = 0;
				break;
			}
		}
		if(sign){
			printf("系统随机数为：%d\n", r);
			break;
		}
	}

	return 0;
}
