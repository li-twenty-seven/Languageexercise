# LanguageExercise

#### 介绍
Hello World!<br/>
学习编程语言！<br/>
锻炼自己的代码能力！

#### 使用环境
Linux环境下，vim编辑器；

JDK 环境下，IDEA编辑器；

分别存放源文件和可执行文件，可以直接运行！


#### C语言
基础练习，熟悉使用。
#### C++
基础练习，熟悉使用。

